#include "Window.h"

Window::Window()
{
    log(0,"+ Window created !\n");
}
Window::~Window()
{
    log(0,"- Window destroyed !\n");
}

int Window::init(int screenW, int screenH, int scale, const char* title)
{
    if (!al_init())
        return log(1,"- Unable to init Allegro 5 !\n");

    if (!al_init_image_addon())
        return log(1,"- Unable to init Addon image !\n");

    if (!al_init_primitives_addon())
        return log(1,"- Unable to init Addon primitives !\n");

    if (!al_init_font_addon())
        return log(1,"- Unable to init Addon font !\n");

    if (!al_init_ttf_addon())
        return log(1,"- Unable to init Addon ttf !\n");

    if (!al_install_keyboard())
        return log(2,"- Unable to install Keyboard !\n");

    if (!al_install_mouse())
        return log(2,"- Unable to install Keyboard !\n");

    _title = title;
    _scaleWin = scale;
    _screenW = screenW;
    _screenH = screenH;
    _isFullScreen = false;
    _displayAdapter = 0;

    _windowDisplay = al_create_display(screenW*scale,screenH*scale);
    _windowBuffer = al_create_bitmap(screenW,screenH);

    al_set_window_title(_windowDisplay, _title);
    setupScreen(_isFullScreen);


    return log(0,"- Init Window ! \n");
}
int Window::done()
{
    if (_windowBuffer != NULL)
        al_destroy_display(_windowDisplay);

    if (_windowDisplay != NULL)
        al_destroy_bitmap(_windowBuffer);

    return log(0,"- Done Window ! \n");
}

void Window::beginRender() const
{
    al_set_target_bitmap(_windowBuffer);
}

void Window::endRender() const
{
    al_set_target_backbuffer(_windowDisplay);
    al_clear_to_color(al_map_rgb(0,0,0));

    if (_isFullScreen)
        al_draw_scaled_bitmap(_windowBuffer,
                              0,0,_screenW,_screenH,
                              _viewX,_viewY,_viewW,_viewH,
                              0);
    else
        al_draw_scaled_bitmap(_windowBuffer,
                              0,0,_screenW,_screenH,
                              0,0,_screenW*_scaleWin,_screenH*_scaleWin,
                              0);
    al_flip_display();
}

void Window::switchMonitor()
{
    _displayAdapter++;
    if (_displayAdapter>al_get_num_video_adapters()-1) _displayAdapter =0;
    delWindow();
    setWindow(_screenW,_screenH,_scaleWin,_title,_displayAdapter);
    setupScreen(_isFullScreen);
}

void Window::changeMonitor(int selectedMonitor)
{
    int activeMonitor( (currentMonitor(getDisplay())) );
    if (selectedMonitor != activeMonitor)
    {
        delWindow();
        setWindow(_screenW,_screenH,_scaleWin,_title,selectedMonitor);
        setupScreen(_isFullScreen);
    }
}

void Window::toggleFullScreen()
{
    int activeMonitor( (currentMonitor(getDisplay())) );
    if (_displayAdapter != activeMonitor)
    {
        _displayAdapter = activeMonitor; // Set index at current Monitor !
        delWindow();
        setWindow(_screenW,_screenH,_scaleWin,_title,_displayAdapter);
    }
    setupScreen(_isFullScreen = !_isFullScreen);
}

int Window::currentMonitor(ALLEGRO_DISPLAY * display) // Find the current monitor where the window is
{
    for (int i(0); i<al_get_num_video_adapters(); i++)
    {
        ALLEGRO_MONITOR_INFO info;
        al_get_monitor_info(i, &info);

        int xWin(0);
        int yWin(0);

        al_get_window_position(display, &xWin, &yWin);

        if (xWin>=info.x1 &&
                yWin>=info.y1 &&
                xWin<info.x2 &&
                yWin<info.y2)
            return i;
    }
    return 0; // Main/Default monitor
}

void Window::pollMouse(ALLEGRO_MOUSE_STATE &mouseState)
{
    if (_isFullScreen)
    {
        _xMouse = (mouseState.x-_viewX)/ _scaleFull;
        _yMouse = (mouseState.y-_viewY)/ _scaleFull;
    }
    else
    {
        _xMouse = mouseState.x/_scaleWin;
        _yMouse = mouseState.y/_scaleWin;

    }
    if (_xMouse < 0)        _xMouse = 0;
    if (_xMouse > _screenW-1) _xMouse = _screenW-1;
    if (_yMouse < 0)        _yMouse = 0;
    if (_yMouse > _screenH-1) _yMouse = _screenH-1;
}

// Getter / Setter !

void Window::setZoom(int zoom)
{
    if (zoom==0)
        zoom=getMaxScale();

    if (_isFullScreen)
        setFullZoom(zoom);
    else
        setWinZoom(zoom);
}

ALLEGRO_DISPLAY * Window::getDisplay()
{
    return _windowDisplay;
}

ALLEGRO_BITMAP * Window::getBuffer()
{
    return _windowBuffer;
}

bool Window::isFullScreen() const
{
    return _isFullScreen;
}

int Window::getDisplayAdapter() const
{
    return _displayAdapter;
}
void Window::setDisplayAdapter(int displayAdapter)
{
    _displayAdapter = displayAdapter;
}
int Window::getScaleWin() const
{
    return _scaleWin;
}

int Window::getScaleFull() const
{
    return _scaleFull;
}

int Window::getViewX() const
{
    return _viewX;
}
int Window::getViewY() const
{
    return _viewY;
}
int Window::getViewW() const
{
    return _viewW;
}
int Window::getViewH() const
{
    return _viewH;
}

int Window::getScreenW() const
{
    return _screenW;
}
int Window::getScreenH() const
{
    return _screenH;
}

int Window::getCurrentMonitorX() const
{
    return _currentMonitorX;
}
int Window::getCurrentMonitorY() const
{
    return _currentMonitorY;
}

int Window::getCurrentMonitorW() const
{
    return _currentMonitorW;
}
int Window::getCurrentMonitorH() const
{
    return _currentMonitorH;
}

float Window::getMouseX() const
{
    return _xMouse;
}
float Window::getMouseY() const
{
    return _yMouse;
}



int Window::setWindow(int screenW, int screenH, int scale, const char* title, int adapter)
{
    al_set_new_display_adapter(adapter);
    _scaleWin = scale;
    _windowDisplay = al_create_display(screenW*scale,screenH*scale);
    al_set_window_title(_windowDisplay, title);

    return log(0,"+++ Window Modified !\n");
}

void Window::delWindow()
{
    if (_windowDisplay!=NULL)
        al_destroy_display(_windowDisplay);

    _windowDisplay = NULL;
}

void Window::setupScreen(bool isFullScreen)
{
    _isFullScreen = isFullScreen;
    al_set_display_flag(_windowDisplay, ALLEGRO_FULLSCREEN_WINDOW, isFullScreen);

    pollMonitor(); // peek Monitor infos !
    _scaleFull = getMaxScale();
    setViewAtCenter(_scaleFull);
}

void Window::setViewAtCenter(int scale) // Place the View in the center of Monitor by scale !
{
    _viewW = _screenW * scale;
    _viewH = _screenH * scale;
    _viewX = (_currentMonitorW - _viewW) / 2;
    _viewY = (_currentMonitorH - _viewH) / 2;
}

int Window::getMaxScale() // Calculate max scaling factor !
{
    _sx = _currentMonitorW / _screenW;
    _sy = _currentMonitorH / _screenH;
    return std::min(_sx, _sy);
}

void Window::pollMonitor()
{
    ALLEGRO_MONITOR_INFO info;
    al_get_monitor_info(_displayAdapter, &info);

    _currentMonitorX = info.x1;
    _currentMonitorY = info.y1;
    _currentMonitorW = info.x2 - info.x1;
    _currentMonitorH = info.y2 - info.y1;
}

void Window::setWinZoom(int scale)
{
    _scaleWin = scale;
    setViewAtCenter(_scaleWin);
    // Replace and resize the Window with the View parameter !
    al_set_window_position(_windowDisplay, _currentMonitorX+_viewX, _currentMonitorY+_viewY);
    al_resize_display(_windowDisplay, _screenW*_scaleWin,_screenH*_scaleWin);

}

void Window::setFullZoom(int scale)
{
    _scaleFull = scale;
    setViewAtCenter(_scaleFull);

}

