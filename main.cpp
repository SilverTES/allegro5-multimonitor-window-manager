
#include "Window.h"

int main()
{
// --- Local Variables ---
    const char * title("--- Multi Monitor Allegro 5 ---");
    int screenW(320);
    int screenH(320);
    int scale(2);
    const char * fontFile("Kyrou.ttf");
    int fontSize(8);

    ALLEGRO_FONT * mainFont(NULL);
    ALLEGRO_MOUSE_STATE _mouseState;
    ALLEGRO_KEYBOARD_STATE _keyState;

    bool _quit(false);
    bool _keyFullScreen(false);
    bool _keyTabScreen(false);

    Window _window;

// --- Init ---
    _window.init(screenW,screenH,scale,title);

// --- Load content ---
    mainFont = al_load_font(fontFile,fontSize,0);
    if (!mainFont) _quit = true;

// --- Begin Main loop ---
    while (!_quit)
    {
    // --- Input ---

        // Keyboard
        al_get_keyboard_state(&_keyState);

        if (al_key_down(&_keyState, ALLEGRO_KEY_ESCAPE)) // Keyboard <Escape> for quit !
            _quit=true;

        if (!al_key_down(&_keyState, ALLEGRO_KEY_TAB)) _keyTabScreen = false;
        if (!al_key_down(&_keyState, ALLEGRO_KEY_F))   _keyFullScreen = false;


        if (al_key_down(&_keyState, ALLEGRO_KEY_TAB) && !_keyTabScreen) // Keyboard <TAB> for switch monitor !
        {
            _keyTabScreen = true;
            _window.switchMonitor();
        }
        if (al_key_down(&_keyState, ALLEGRO_KEY_F) && !_keyFullScreen) // Keyboard <F> to enter fullscreen !
        {
            _keyFullScreen = true;
            _window.toggleFullScreen();
        }

        if (al_key_down(&_keyState, ALLEGRO_KEY_PAD_0)) _window.setZoom(0); // 0 = Zoom max
        if (al_key_down(&_keyState, ALLEGRO_KEY_PAD_1)) _window.setZoom(1);
        if (al_key_down(&_keyState, ALLEGRO_KEY_PAD_2)) _window.setZoom(2);
        if (al_key_down(&_keyState, ALLEGRO_KEY_PAD_3)) _window.setZoom(3);

        // Mouse
        al_get_mouse_state(&_mouseState);
        _window.pollMouse(_mouseState);

    // --- Render ---

        _window.beginRender();
        {
            al_clear_to_color(al_map_rgb(25,30,50));

            int _windowX(0);
            int _windowY(0);

            al_get_window_position(_window.getDisplay(), &_windowX, &_windowY);

            int _windowW(al_get_display_width(_window.getDisplay()));
            int _windowH(al_get_display_height(_window.getDisplay()));

            al_draw_textf(mainFont, al_map_rgb(25,250,200), 200, 2, 0,
                          "_isFullScreen : %i", _window.isFullScreen());

            al_draw_textf(mainFont, al_map_rgb(25,250,200), 200, 22, 0,
                          "_viewX : %i", _window.getViewX() );

            al_draw_textf(mainFont, al_map_rgb(25,250,200), 200, 32, 0,
                          "_viewY : %i", _window.getViewY() );

            al_draw_textf(mainFont, al_map_rgb(25,250,200), 200, 42, 0,
                          "_scaleFull : %i", _window.getScaleFull() );
            al_draw_textf(mainFont, al_map_rgb(25,250,200), 200, 52, 0,
                          "_scaleWin  : %i", _window.getScaleWin() );


            al_draw_textf(mainFont, al_map_rgb(25,250,200), 20, 122, 0,
                          "_currentMonitor POS  : %i, %i", _window.getCurrentMonitorX(), _window.getCurrentMonitorY());
            al_draw_textf(mainFont, al_map_rgb(25,250,200), 20, 132, 0,
                          "_currentMonitor Size : %i, %i", _window.getCurrentMonitorW(), _window.getCurrentMonitorH());

            al_draw_textf(mainFont, al_map_rgb(205,250,200), 4, 2, 0,
                          "currentMonitor : %i", _window.currentMonitor(_window.getDisplay()));

            al_draw_textf(mainFont, al_map_rgb(205,250,200), 4, 12, 0,
                          "_displayAdapter : %i", _window.getDisplayAdapter());

            al_draw_textf(mainFont, al_map_rgb(250,120,50), 4, 36, 0,
                          "Window Pos : %i , %i", _windowX, _windowY);

            al_draw_textf(mainFont, al_map_rgb(0,250,250), 4, 46, 0,
                          "Window Size : %i , %i", _windowW, _windowH);

            al_draw_textf(mainFont, al_map_rgb(50,150,250), 4, 106, 0,
                          "Mouse Pos : %i , %i", int(_window.getMouseX()), int(_window.getMouseY()));

            al_draw_line(0,_window.getMouseY()+.5,
                         screenW,_window.getMouseY()+.5,
                         al_map_rgba(55,155,100,25),0);

            al_draw_line(_window.getMouseX()+.5,0,
                         _window.getMouseX()+.5,screenH,
                         al_map_rgba(55,155,100,25),0);
        }
        _window.endRender();

    }
// --- End Main loop ---

// --- Done ---

    _window.done();
    al_destroy_font(mainFont);

// --- Quit program !
    return log(0,"- Program terminated normally !\n");
}
