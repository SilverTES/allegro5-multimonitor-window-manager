#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <iostream>
#include <vector>
#include <memory>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_windows.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_windows.h>

template <class E, class M>
E log (E error, M msg)
{
    std::cout << msg;
    return error;
}

class Window
{
    public:

        Window();
        virtual ~Window();

        int init(int screenW, int screenH, int scale, const char* title);
        int done();

        void beginRender() const;
        void endRender() const;

        void switchMonitor();
        void changeMonitor(int selectedMonitor);
        void toggleFullScreen();
        int currentMonitor(ALLEGRO_DISPLAY * display); // Find the current monitor where the window is
        void pollMouse(ALLEGRO_MOUSE_STATE &mouseState);

        // Getter / Setter !
        ALLEGRO_DISPLAY * getDisplay();
        ALLEGRO_BITMAP * getBuffer();

        void setZoom(int zoom);
        bool isFullScreen() const;
        void setDisplayAdapter(int displayAdapter);
        int getDisplayAdapter() const;
        int getScaleWin() const;
        int getScaleFull() const;
        int getViewX() const;
        int getViewY() const;
        int getViewW() const;
        int getViewH() const;
        int getScreenW() const;
        int getScreenH() const;
        int getCurrentMonitorX() const;
        int getCurrentMonitorY() const;
        int getCurrentMonitorW() const;
        int getCurrentMonitorH() const;
        float getMouseX() const;
        float getMouseY() const;

    protected:

        int setWindow(int screenW, int screenH, int scale, const char* title, int adapter);
        void delWindow();
        void setupScreen(bool isFullScreen);
        void setViewAtCenter(int scale); // Place the View in the center of Monitor by scale !
        int getMaxScale(); // Calculate max scaling factor !
        void pollMonitor();
        void setWinZoom(int scale);
        void setFullZoom(int scale);

        // Window
        const char* _title;
        int _displayAdapter;
        bool _isFullScreen;
        int _screenW;
        int _screenH;

        // Windowed
        int _scaleWin;

        // FullScreen
        int _scaleFull;
        int _sx;
        int _sy;
        int _viewW;
        int _viewH;
        int _viewX;
        int _viewY;

        // Monitor
        int _currentMonitorX;
        int _currentMonitorY;
        int _currentMonitorW;
        int _currentMonitorH;

        // Mouse
        float _xMouse;
        float _yMouse;

    private:

        ALLEGRO_DISPLAY * _windowDisplay;
        ALLEGRO_BITMAP * _windowBuffer;
};


#endif // WINDOW_H_INCLUDED
